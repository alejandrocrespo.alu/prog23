// Programa que fa una còpia del seu propi codi font
import java.io.*;

public class copiaByteAByte
{
	public static void main(String[] args) {
			File f = new File("copiaByteAByte.java");
			if (f.exists())
			{
				try
				{
					// objecte per a lectura (origen de la còpia)
					FileInputStream fis = new FileInputStream(f);
					// objecte per a escritura (destí de la còpia)
					FileOutputStream fos = new FileOutputStream("copia.txt");
					// Llegiré cada byte i el mostraré per pantalla
					char c;
					c=(char)fis.read();
					while ( c != (char)-1 )
					{
						//System.out.print(c);	// mostre el caracter i continue llegint el següent
						// escric el caracter al fos
						fos.write(c);
						c = (char)fis.read();
					}
					fis.close();
					fos.close();
				}/*
				catch(FileNotFoundException e)
				{
					System.err.print("Excepció per no existir el fitxer");
				}*/
				catch(IOException e)
				{
					System.err.println(e.getMessage());
				}
			}
			else
				System.out.println("El fitxer no existeix");
			
		}	
}