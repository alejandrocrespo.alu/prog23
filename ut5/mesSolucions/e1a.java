/* 1. Programa que demane 5 dates en format (dia,mes,any) i les escriga a fitxer en les següents formes:

	a) fitxer de text (en format 21/3/2022, cada data en una línia) */

import java.io.*;
import java.util.Scanner;
class e1a  {
	private static final int COUNT = 5;
	public static void main(String[] args) {
		int dia,mes,any;
		Scanner ent = new Scanner(System.in);
		try(FileWriter fw = new FileWriter("dates.txt"))
		{
			for(int i= 0 ; i < COUNT ; i++ )
			{
				System.out.println("Introduix dia, mes i any");
				dia = ent.nextInt();
				mes = ent.nextInt();
				any = ent.nextInt();
				// escriure la línia a fitxer
				fw.write(dia + "/" + mes + "/" + any + "\n");
			}
		}
		catch(IOException e)
		{
			System.err.println(e.getMessage());
		}
	}
}