; Programa que mostre de 1 a 10, a la manera de la programació imperativa
;(def n 1)

(loop [n 1]
	(if (< n 11)
		(do
			(println n)
			(recur (inc n))
		)
	)
)
