;1. Funció "primeraLletra" que retorne el primer caracter d'un text

(defn primeraletra
"Obté la primera lletra d'una texta"
[x]
    (.charAt x 0)

)
(println "introduce una palabra")
(def text (read-line))
(println "la primera letra de" text " es:" (primeraletra text))