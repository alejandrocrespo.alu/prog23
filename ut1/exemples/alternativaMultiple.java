/* Programa que demane un enter corresponent a un dia de la setmana i indique el dia textual (1=Dilluns, 2=dimarts, ..., 7=diumenge) */

public class alternativaMultiple
{
    public static void main(String args[])
    {
        int dia;

        System.out.println("Introduïx dia de la setmana (de 1 a 7):");
        dia = Integer.parseInt(System.console().readLine());
        //switch (dia%7)
        switch (dia)
        {
            case 1: System.out.println("Dilluns");break;
            case 2: System.out.println("Dimarts");break;
            case 3: System.out.println("Dimecres");break;
            case 4: System.out.println("Dijous");break;
            case 5: System.out.println("Divendres");break;
            case 6: System.out.println("Disabte");break;
            case 7: System.out.println("Diumenge");break;
            default: System.out.println("Dia incorrecte");
        }

    }
}
