// Exemple d'alternativa simple (if): programa que indicarà si el valor introduït és positiu

public class alternativaDoble
{
    public static void main(String args[])
    {
        double num;

        System.out.println("Introduïx un número:");
        num = Double.parseDouble(System.console().readLine());
        if ( num >= 0) // NO PUNT I COMA
            System.out.println("És positiu");
        else    // si no es complix la condició
        {
            System.out.println("És negatiu");
            System.out.println("Si volies introduïr un positiu torna a executar el programa"); 
        }   // tanca l'else'      
    }   // tanca main
}   // tanca la classe
