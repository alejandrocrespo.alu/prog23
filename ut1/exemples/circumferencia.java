//

public class circumferencia
{
    public static void main(String args[])
    {
        final double PI = 3.1416;   // és una constant, no podré canviar el seu valor
        double radi;

        System.out.println("Introduïu el radi de la circumferència:");
        radi = Double.parseDouble(System.console().readLine());
        System.out.println("El seu perímetre és " + 2 * PI * radi);
    }
}
