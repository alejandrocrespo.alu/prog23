//26. Algoritme que indique si un nombre introduït per teclat és primer.

public class e26c
{
  public static void main(String args[])
  {
    int a, i;
    System.out.print("Introduzca un número entero mayor que 1 para comprobar si es primo: ");
    a = Integer.parseInt(System.console().readLine());
	System.out.println(a);
	if (a < 2)
		System.out.println("Com a mínim un 2");
	else
	{	
	    for(i = 2; (a % i) != 0  ;i++);
		// quan acabe el bucle
	    if (i < a)
	    	System.out.println("NO és primer");
	    else
	    	System.out.println("SI és primer");
    	}

  }
}
