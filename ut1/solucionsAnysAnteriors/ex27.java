//27. Modificar l'anterior algoritme perquè demane una llista d'enters acabada amb un 0 i indique, per a cada un d'ells, si és primer o no. A més, a la fi ha d'indicar quants primers s'han introduït.

public class ex27
{
	public static void main(String args[])
	{
		int n, div=1, cont=2, cont1=0;
		System.out.println("Introdueix un número (0 per a acabar)");
		n = Integer.parseInt(System.console().readLine());
		
		while (n != 0)
		{
		
			do 
			{
				if (n == 2)
					break;
				div = n % cont;
				//System.out.println("DIV "+div);
				cont++;
				//System.out.println("CONT "+cont);
			}	
			while ((cont < n) && (div != 0));

			if (n<0)
			{
				System.out.println("Introdueix un número (0 per a acabar)");
				n = Integer.parseInt(System.console().readLine());
				cont=2;
				div=1;
				continue;
			}
			if (div==0)
				System.out.println("El número no és primer");
			else // div != 0
			{
				System.out.println("El número és primer");
				cont1++;
			}
			System.out.println("Introdueix un número (0 per a acabar)");
			n = Integer.parseInt(System.console().readLine());
			cont=2;
			div=1;
		} //Llegir i esceriure abans de tornar a avaluar la condició
		System.out.println("Han hagut en total "+cont1+" nombres primers");
	}
}
