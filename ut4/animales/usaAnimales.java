import java.util.Scanner;
import animal.*;

public class usaAnimales
{
	public static void main(String[] args) 
	{
		Scanner ent=new Scanner(System.in);
		char op;
		
		gato g1=new gato();
		elefante e1=new elefante("Verde",23,8);
		gato g2=new gato("Rojo",12,20);
		
		System.out.println(g1);
		System.out.println(e1);
		System.out.println(g2);
		
		System.out.print("Quieres castrar al gato?(s/n): ");
		op=ent.nextLine().charAt(0);	
		
		if(op=='s')
			g1.castrar();
		else
		{
			System.out.println("Lo voy a castrar aunque digas que no");
			g1.castrar();
		}
		
		System.out.println(g1);
	}
}
