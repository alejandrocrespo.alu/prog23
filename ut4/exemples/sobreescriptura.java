class A {
	int i, j;
	A (int a, int b) {
		i = a;
		j = b;
	}
	// S'imprimeixen i i j.
	void show() {
		System.out.println ( "i i j:" + i + " " + j);
	}
}
class B extends A {
	int k;
	B (int a, int b, int c) {
		super(a, b);
		k = c;
	}
	// S'imprimeix k sobreescrivint el mètode () a A.
	@Override
	void show(int num) {
		super.show();
		System.out.println( "k:" + k);
	}
}

public class sobreescriptura
{
	public static void main(String args[])
	{
		B b = new B(1,2,3);
		b.show();
	}
}
