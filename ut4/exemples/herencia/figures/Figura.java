// Classe BASE

package figures;

public class Figura
{
	protected String color;
	
	public Figura() { color = "negre";}
	
	public Figura(String c) { color = c; }
	
	public void setColor(String c) { color = c; }
	
	public String getColor() { return color; }
	
	public String toString() { return "El color de la figura és " + color; }
}
