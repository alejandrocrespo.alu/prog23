/* 1. Crear una classe Rellotge (hora,minuts,segons) que permeta almenys:

 - crear rellotges amb hora inicial les 12 del migdia
 - crear rellotges amb hora inicial a triar
 - canviar només l'hora, només els minuts o només els segons (setters)
 - obtindre només el valor d'hora, o de minuts o de segons (getters)
 - obtindre els segons transcorreguts des de les 12 de la mitjanit()
 - afegir una quantitat de temps expressada en segons (vigila que els minuts o segons no excedisquen de 59, ni les hores de 23).
 	Exemple:
 	si el rellotge marca les 10.35:24 i se li afigen 1810 segons (mitja hora i 10 segons) quedarà en 11.05:34
 - ... (altres operacions que se t'ocórreguen útils)
 
 Crea un programa que instàncie rellotges posant a prova tota la classe anterior */

public class Rellotge{

    private int hora;
    private int minut;
    private int segon;
    private boolean error = false;

    public Rellotge(){
        hora = 12;
        minut = 0;
        segon = 0;
    }

    public Rellotge(int h, int m, int s){
        hora = h;
        minut = m;
        segon = s;
    }

    public Rellotge(Rellotge rel){
        hora = rel.hora;
        minut = rel.minut;
        segon = rel.segon;
    }

    public void setRellotge(int h, int m, int s){
        hora = h;
        minut = m;
        segon = s;
    }

    public void setHora(int h){
        hora = h;
    }

    public void setMinut(int m){
        minut = m;
    }

    public void setSegon(int s){
        segon = s;
    }

    public int getHora(){
        return hora;    
    }

    public int getMinut(){
        return minut;
    }

    public int getSegon(){
        return segon;
    }

    public void difMN(){
        System.out.println("Han passat " + ((hora *= 3600) + (minut *= 60) + segon) + " segons desde mitjanit");
    }

    public void afigSeg(int s/*7264*/){
       /* hora = (s / 3600) + getHora();
        minut = (s % 3600) / 60 + getMinut(); */
       /* hora = getHora() + getHora(s / 3600);
        minut = getMinut() + getMinut((s % 3600) / 60); */
        hora = (s / 3600) + getHora() / 3600;
        minut = (s % 3600) / 60 + getMinut() / 60;
        segon = (s % 3600) % 60 + getSegon(); 
    }

    public void hora(){
        if (segon > 59){
            segon -= 60;
            minut++;
        }
        if (minut > 59){
            minut -= 60;
            hora++;
        }
        if (hora > 23)
            hora -= 24;
        if (hora < 10)
            System.out.print("0" + hora + ":");
        else
            System.out.print(hora + ":");
        
    }

    public void minut(){
        if (minut < 10)
            System.out.print("0" + minut + ":");
        else
            System.out.print(minut + ":");
    }

    public void segon(){
        if (segon < 10)
            System.out.print("0" + segon + "\n");
        else
            System.out.print(segon + "\n");
    }

    public void check(){
        if ((segon < 0) || (minut < 0) || (hora < 0))
            error = true;
    }

    public void mostraRellotge(){
        check();
        if (error == true){
            System.out.println("ERROR");
            return;
        }
        System.out.print("Són les ");
        hora();
        minut();
        segon();
    }

}
