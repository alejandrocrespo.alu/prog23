/*2. Escriure un programa que reba com a dades una cadena de caràcters i un caràcter i reporta el nombre de vegades que es troba el caràcter en la cadena.*/ 
//Resuelto con paso de parámetres a main
public class ej2String
{
	public static void main(String args[])
	{
		int cont=0;
		if(args.length < 2)
			System.out.println("Forma de uso: java ejString2 \"Cadena de texto y \"caracter ");
		else
		{
			String text=args[0];
			char arrayc[]=text.toCharArray();//Conviertes el String en un array de caracteres
			char c = args[1].charAt(0);
			for(int i=0;i<arrayc.length;i++)
			{
				if(arrayc[i]==c)
					cont++;
			}
			System.out.println("La letra aparece "+cont+ " veces");
		}
			
	}	
}
