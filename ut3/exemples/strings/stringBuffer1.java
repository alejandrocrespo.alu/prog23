public class stringBuffer1
{
	public static void main(String args[])
	{
		// crea un StringBuffer a partir d'un String "dam1"
		StringBuffer sb = new StringBuffer("dam1");
		sb.reverse();
		System.out.println(sb);
		sb.insert(1,'a');
		System.out.println(sb);
		sb.insert(2,"bcdef");
		System.out.println(sb);
		sb.delete(4,7);
		System.out.println(sb);
		sb.setCharAt(4,'M');
		System.out.println(sb);
		// Obtindre un String a partir del StringBuffer
		String s = sb.substring(0);
		System.out.println("Convertit a String: " + s);
		// Ara a la inversa, de String a StringBuffer
		sb = new StringBuffer(s);
		
		
	}
}
